<?php

namespace EasySwoole\EasySwoole;

use App\WeChat\Message\OnMessageScan;
use App\WeChat\Message\OnMessageSubscribe;
use App\WeChat\Message\OnMessageText;
use App\WeChat\Message\OnMessageUnsubscribe;
use App\WeChat\Message\OnSentEvent;
use App\WeChat\WeChatManager;
use EasySwoole\EasySwoole\AbstractInterface\Event;
use EasySwoole\EasySwoole\Swoole\EventRegister;
use EasySwoole\HotReload\HotReload;
use EasySwoole\HotReload\HotReloadOptions;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;
use EasySwoole\WeChat\Bean\OfficialAccount\RequestConst;
use EasySwoole\WeChat\Bean\OfficialAccount\RequestMsg;
use EasySwoole\WeChat\WeChat;

class EasySwooleEvent implements Event
{
    public static function initialize()
    {
        include_once __DIR__ . '/App/common.php';
        date_default_timezone_set('Asia/Shanghai');
    }

    public static function mainServerCreate(EventRegister $register)
    {
        self::setWeChat();
        self::setHotReLoad();
    }

    public static function onRequest(Request $request, Response $response): bool
    {
        return true;
    }

    public static function afterRequest(Request $request, Response $response): void
    {

    }

    private static function setWeChat()
    {
        $weChatConfig = new \EasySwoole\WeChat\Config();
        $weChatConfig->setTempDir(Config::getInstance()->getConf('TEMP_DIR'));
        $weChatConfig->officialAccount()->setAppId('wx0c0d7f7cfd1af47f');
        $weChatConfig->officialAccount()->setAppSecret('bd690dae00fa8c151b98780b3a3a6448');
        $weChatConfig->officialAccount()->setToken('fwe3233FsDE');
        $weChatConfig->officialAccount()->setAesKey('gRDr65HXQogqa1QkLRM8LfmJmaZFfd2QgcLEe8zEwDX');
        // 注册WeChat对象到全局List
        WeChatManager::getInstance()->register('default', new WeChat($weChatConfig));
        $server = WeChatManager::getInstance()->weChat('default')->officialAccount()->server();
        //文字回复
        $server->onMessage()->set(RequestConst::MSG_TYPE_TEXT, function (RequestMsg $msg) {
            return OnMessageText::handle($msg);
        });
        //关注事件
        $server->onEvent()->onSubscribe(function (RequestMsg $msg) {
            return OnMessageSubscribe::handle($msg);
        });
        //取消关注事件
        $server->onEvent()->onUnSubscribe(function (RequestMsg $msg) {
            return OnMessageUnsubscribe::handle($msg);
        });
        //订阅事件
        $server->onEvent()->onSent(function (RequestMsg $msg) {
            echo "触发订阅事件";
            return OnSentEvent::handle($msg);
        });

        $server->onEvent()->onTwoSent(function (RequestMsg $msg) {
            echo "触发二次订阅事件";
            var_dump($msg);
            return OnSentEvent::handle($msg);
        });
        //取消订阅事件
        $server->onEvent()->onUnSent(function (RequestMsg $msg) {
            echo "触发取消订阅事件";
            return OnSentEvent::unHandle($msg);
        });
        //扫码事件
        $server->onEvent()->onScan(function (RequestMsg $msg) {
            return OnMessageScan::handle($msg);
        });
    }

    //本地开发热重启
    private static function setHotReLoad()
    {
        if (is_local_dev()) {
            $hotReloadOptions = new HotReloadOptions();
            $hotReload        = new HotReload($hotReloadOptions);
            $hotReloadOptions->setMonitorFolder([EASYSWOOLE_ROOT . '/App']);
            $server = ServerManager::getInstance()->getSwooleServer();
            $hotReload->attachToServer($server);
       }
    }
}