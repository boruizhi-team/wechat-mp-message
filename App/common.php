<?php

/**
 * 获取env环境变量
 * @param string $key
 * @param $default
 * @return mixed
 */
function env_get(string $key, $default = '')
{
    $env = M1\Env\Parser::parse(file_get_contents(__DIR__ . '/../.env'));
    return $env[$key] ?? $default;
}

/**
 * 有捕获的go函数（协程）
 * @param callable $func
 */
function sgo(callable $func)
{
    try {
        go($func);
    } catch (Throwable $t) {
        echo $t->getMessage(), PHP_EOL;
    }
}

/**
 * 生成链接文字
 * @param string $linkName
 * @param string $url
 * @return string
 */
function linkText(string $linkName, string $url): string
{
    return "<a href='{$url}'>{$linkName}</a>";
}

/**
 * 是否是本地开发环境
 * @return bool
 */
function is_local_dev(): bool
{
    $local = env_get('local', 0);
    return $local == 1;
}
