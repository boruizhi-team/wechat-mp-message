<?php

namespace App\Model;


use EasySwoole\Mysqli\Exception\Exception;

class BindLogModel
{
    private static string $table = 'brz_bind_consultant_log';

    public function insert(array $data)
    {
        $mysqlFac = new MysqliFactory();
        $client = $mysqlFac->createMysqlClient();
        $client->queryBuilder()->insert(self::$table, $data);
        try {
            $client->execBuilder();
        } catch (Exception $e) {
        } catch (\Throwable $e) {
            echo $e->getMessage(), PHP_EOL;
        }
    }
}