<?php

namespace App\Model;

use EasySwoole\Mysqli\Client;

/**
 * Mysqli工厂类
 *
 * Class MysqliFactory
 * @package App\Util
 */
class MysqliFactory
{
    /**
     * 获取Mysql client
     *
     * @return Client
     */
    public function createMysqlClient()
    {
        $config = new \EasySwoole\Mysqli\Config([
            'host' => env_get('db_hostname'),
            'port' => env_get('db_hostport'),
            'user' => env_get('db_username'),
            'password' => env_get('db_password'),
            'database' => env_get('db_database'),
            'timeout' => 5,
            'charset' => 'utf8mb4',
        ]);

        return new Client($config);
    }
}

