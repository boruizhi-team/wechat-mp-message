<?php

namespace App\Model;


class AutoReplyModel
{
    private static string $table = 'brz_mp_auto_reply';

    //类型-关注自动回复
    const TYPE_SUBSCRIBE = 0;

    //类型-发送文字消息自动回复
    const TYPE_SEND_TEXT = 1;

    public function getReply(int $type): string
    {
        $time = time();
        $mysqlFac = new MysqliFactory();
        $client = $mysqlFac->createMysqlClient();
        $client->queryBuilder()
            ->where("$time >= apply_time")
            ->where('status', 1)
            ->where('type', $type)
            ->orderBy('apply_time', 'DESC')
            ->limit(1)
            ->get(self::$table);
        $ret = $client->execBuilder();
        if (isset($ret[0]) && isset($ret[0]['content'])) {
            return $ret[0]['content'];
        } else {
            if ($type == self::TYPE_SUBSCRIBE) {
                $default = $this->defaultReply();
            } else if ($type == self::TYPE_SEND_TEXT) {
                $default = $this->defaultTextSendReply();
            } else {
                $default = '';
            }
            return $default;
        }
    }

    private function defaultReply(): string
    {
        return "欢迎您关注博瑞智家庭教育官方公众号！\n
        👉周一至周五早课堂，我们语音为您分享家庭教育相关精品文章\n
        👉每月一线家庭教育名师免费直播授课，分享热门家庭教育知识\n
        ❤我们为你准备了《早课堂精选20期——董进宇博士原创文章合集》，邀请【3】位好友扫码助力，即可0元领取。\n
      <a href=\"https://mini.dongjinyu.com/wechat/mainv2/book_list\">参与活动，请戳这里</a>";
    }

    private function defaultTextSendReply(): string
    {
        return '我们已收到您的消息，稍后将会回复，如有任何疑问请拨打400-100-9330';
    }
}