<?php

namespace App\Model;
class UserModel
{
    private static string $table = 'brz_user';

    /**
     * 设置用户关注公众号状态
     *
     * @param string $openid
     * @param bool $isSubscribe
     */
    public function setSubscribeStatus(string $openid, bool $isSubscribe)
    {
        $mysqlFac = new MysqliFactory();
        $client = $mysqlFac->createMysqlClient();
        $client->queryBuilder()
            ->where('mp_openid', $openid)
            ->update(self::$table, ['is_subscribe' => $isSubscribe ? 1 : 0]);
        try {
            $client->execBuilder();
        } catch (\Throwable $exception) {
            echo $exception->getMessage() . PHP_EOL;
        }
    }

    public function setSubscription(string $openid, bool $isSubscription)
    {
        $mysqlFac = new MysqliFactory();
        $client = $mysqlFac->createMysqlClient();
        $client->queryBuilder()
            ->where('mp_openid', $openid)
            ->update(self::$table, ['is_subscription' => $isSubscription ? 1 : 0]);
        try {
            $client->execBuilder();
        } catch (\Throwable $exception) {
            echo $exception->getMessage() . PHP_EOL;
        }
    }

    /**
     * 是否是会员
     * @param int $uid
     * @return bool
     */
    public function isMember(int $uid): bool
    {
        try {
            $client = (new MysqliFactory())->createMysqlClient();
            $client->queryBuilder()->where('id', $uid)->limit(1)->getColumn(self::$table, 'member_level');
            $ret = $client->execBuilder();
            if (!$ret) {
                return false;
            }
            $memberLevel = $ret[0]['member_level'];
            return boolval($memberLevel == 1);
        } catch (\Throwable $e) {
            echo $e->getMessage(), PHP_EOL;
            return false;
        }
    }

    public function getColumnByUid(int $uid, $fields)
    {
        $client = (new MysqliFactory())->createMysqlClient();
        $client->queryBuilder()->where('id', $uid)->fields($fields)->limit(1)->get(self::$table);
        try {
            $ret = $client->execBuilder();
            return $ret[0] ?? null;
        } catch (\Throwable $e) {
            return null;
        }
    }

    public function getColumnByOpenid(string $openid, $fields)
    {
        $client = (new MysqliFactory())->createMysqlClient();
        $client->queryBuilder()->where('mp_openid', $openid)->fields($fields)->limit(1)->get(self::$table);
        try {
            $ret = $client->execBuilder();
            return $ret[0] ?? null;
        } catch (\Throwable $e) {
            return null;
        }
    }

    public function getUidByOpenId(string $openid): int
    {
        $client = (new MysqliFactory())->createMysqlClient();
        $client->queryBuilder()->where('mp_openid', $openid)->fields(['id'])->limit(1)->get(self::$table);
        try {
            $ret = $client->execBuilder();
            return intval($ret[0]['id']);
        } catch (\Throwable$e) {
            return 0;
        }
    }

    public function updateById(int $id, $data)
    {
        $client = (new MysqliFactory())->createMysqlClient();
        $client->queryBuilder()->where('id', $id)->update(self::$table, $data);
        try {
            $ret = $client->execBuilder();
            var_dump($ret);
        } catch (\Throwable $e) {

        }
    }
}