<?php

namespace App\Model;
class MpQrDefModel
{
    private string $table = 'brz_mp_qr_def';

    public function findById(int $id)
    {
        $client = (new MysqliFactory())->createMysqlClient();
        $client
            ->queryBuilder()
            ->where('id', $id)
            ->where('status',1)
            ->get($this->table);
        try {
            return $client->execBuilder()[0] ?? null;
        } catch (\Throwable $e) {
            echo $e->getMessage(), PHP_EOL;
            return null;
        }
    }


}