<?php

namespace App\Model;
class MpTextResModel
{
    /**
     *
     * @param $keyword string 关键字
     * @return \EasySwoole\Mysqli\QueryBuilder|null
     */
    public function getTextRes(string $keyword)
    {
        $factory = new MysqliFactory();
        $client = $factory->createMysqlClient();
        $client
            ->queryBuilder()
            ->where('keyword', $keyword)
            ->fields(['type','title', 'url', '`desc`'])
            ->get('brz_mp_text_res');

        try {
            $data = $client->execBuilder()[0] ?? null;
        } catch (\Throwable $exception) {
            echo $exception->getMessage() . PHP_EOL;
            return null;
        }
        return $data;
    }

}

