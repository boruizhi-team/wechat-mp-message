<?php

namespace App\Model;
class MpInvitedModel
{
    private static string $table = 'brz_mp_qr_def_invited';

    public function findByOpenid(string $openid, array $fields)
    {
        $client = (new MysqliFactory())->createMysqlClient();
        $client->queryBuilder()->where('openid', $openid)->fields($fields)->get(self::$table);
        try {
            $ret = $client->execBuilder();
            if (!$ret) {
                return [];
            }
            return $ret[0];
        } catch (\Throwable $e) {
            echo $e->getMessage(), PHP_EOL;
            return [];
        }
    }

    public function insert(array $data)
    {
        $client = (new MysqliFactory())->createMysqlClient();
        $client->queryBuilder()->insert(self::$table, $data);
        try {
            $client->execBuilder();
        } catch (\Throwable $e) {
            echo $e->getMessage(), PHP_EOL;
        }
    }
}