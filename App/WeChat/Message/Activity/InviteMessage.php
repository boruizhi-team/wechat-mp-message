<?php

namespace App\WeChat\Message\Activity;

class InviteMessage
{
    //活动结束消息
    static function activityEndMessage(string $nickname): string
    {
        return "亲爱的{$nickname}，你来晚啦，本次活动已经结束啦~\n
        请持续关注我们，后续还有更多福利活动哦！";
    }

    //A任务已完成，B输入邀请码时给B的提示
    static function onACompleteButBSendCodeMessage(string $nickname, string $gift): string
    {
        $link = linkText('请点击蓝色字进入立即领取>>>', Activity::$pageUrl);
        return "你的好友{$nickname}已完成助力，\n
        成功领取到《{$gift}》\n
        若您也想免费领取《{$gift}》\n
        {$link}";
    }

    //B在活动开始前关注公众号的提示
    static function bIsOldUserMessage(): string
    {
        $link = linkText('请点击蓝色字进入立即领取>>>', Activity::$pageUrl);
        return "你已经是我们的老朋友，无法为朋友助力！\n
        我们真诚邀请你参加0元领福利活动\n
        {$link}";
    }

    //A助力完成
    static function onATaskCompleteMessage(string $friendNickname, int $targetNeed, int $currNeed, string $giftName, int $code, int $giftType): string
    {
        if ($giftType == 0) {
            //书
            $url  = "https://mini.dongjinyu.com/wechat/mainv2/book_list?gift_code=$code&gift_name=$giftName";
            $link = linkText('点击蓝字填写收货地址>>>', $url);
        } else {
            //课
            $url  = "https://mini.dongjinyu.com/wechat/mainv2/my_class?gift_code=$code&gift_name=$giftName";
            $link = linkText('点击蓝字领取课程>>>', $url);
        }
        return "你的好友{$friendNickname}为你成功助力！\n
        助力目标：{$targetNeed}人\n
        已完成助力：{$currNeed}人\n
        恭喜你，已成功领取到《{$giftName}》\n\n
        {$link}";
    }

    //B在活动进行时取消关注，给A的通知
    static function onBUnSubscribeAndInActivityMessage(string $friendNickname, int $targetNeed, int $currNeed, string $giftName): string
    {
        $sub = $targetNeed - $currNeed;
        return "对不起，你的好友{$friendNickname}取消关注了公众号，助力人数-1\n
        助力目标：{$targetNeed}人\n
        已完成助力：{$currNeed}人\n
        你还差{$sub}人即可免费领取《{$giftName}》，加油！";
    }

    //B重复给A助力(重复输入邀请码)
    static function onBRepeatActionMessage(string $nickname, string $giftName): string
    {
        $link = linkText('请点击蓝色字进入立即领取>>>', Activity::$pageUrl);
        return "你已为好友{$nickname}助力过，无法再次助力！\n
        若你也想免费领取《{$giftName}》\n
        {$link}";
    }

    //B助力后，A的消息
    static function onInviteSuccessForAMessage(string $nickname, int $targetNeed, int $currNeed, string $giftName): string
    {
        $sub = $targetNeed - $currNeed;
        return "{$nickname}已为你助力成功，快去谢谢TA吧！\n
        助力目标：{$targetNeed}人\n
        已完成助力：{$currNeed}人\n
        你还差{$sub}人即可免费领取《{$giftName}》，加油！";
    }

    //B助力后，B的消息
    static function onInviteSuccessForBMessage(string $nickname, string $giftName): string
    {
        $link = linkText('请点击蓝色字进入立即领取>>>', Activity::$pageUrl);
        return "你已成功为好友{$nickname}助力！\n
        若您也想免费领取《{$giftName}》\n
        {$link}";
    }

    //A扫码的文字消息
    static function onAScanTextMessage(string $giftName, int $need): string
    {
        $link = linkText('点击蓝字了解详情>>>', Activity::$pageUrl);
        return "免费领《{$giftName}》\n\n
        领取方式：\n
        1、长按下方图片并转发给好友，让好友扫码进入公众号，回复你的邀请码，即算1次助力\n
        2、邀请{$need}个好友助力成功，即可免费领取\n
        {$link}";
    }
}
