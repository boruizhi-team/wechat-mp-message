<?php

namespace App\WeChat\Message;

use App\Model\UserModel;
use EasySwoole\WeChat\Bean\OfficialAccount\RequestMsg;

class OnSentEvent
{
   public static function handle(RequestMsg $msg)
    {
        // 创建一个UserModel实例
        $userModel = new UserModel();
        // 设置订阅状态为true
        $userModel->setSubscription($msg->getFromUserName(), true);
        // 返回null
        return null;
    }

    public static function unHandle(RequestMsg  $msg){
        // 创建一个UserModel实例
        $userModel = new UserModel();
        // 设置订阅状态为false
        $userModel->setSubscription($msg->getFromUserName(), false);
        // 返回null
        return null;
    }
}