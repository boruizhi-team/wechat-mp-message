<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'guanxi/wechat-mp-message-handler',
  ),
  'versions' => 
  array (
    'dragonmantank/cron-expression' => 
    array (
      'pretty_version' => 'v2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '72b6fbf76adb3cf5bc0db68559b33d41219aba27',
    ),
    'easyswoole/component' => 
    array (
      'pretty_version' => '2.2.1',
      'version' => '2.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '342b94d6f259e1abdb3262f4afe0ea29daefbd44',
    ),
    'easyswoole/config' => 
    array (
      'pretty_version' => '1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bdb566ea7ef165c337d7fc6d2d7cf3538233d8a6',
    ),
    'easyswoole/easyswoole' => 
    array (
      'pretty_version' => '3.3.4',
      'version' => '3.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d73bdb1d7884ef629992fc95f7c0a59519231b4',
    ),
    'easyswoole/hot-reload' => 
    array (
      'pretty_version' => '0.1.2',
      'version' => '0.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c13a1222cec4e3c1df4c01ac51968cefe63dfc01',
    ),
    'easyswoole/http' => 
    array (
      'pretty_version' => '1.6.1',
      'version' => '1.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '62921096e00a91e5c0128e29424bbe0bc0229201',
    ),
    'easyswoole/http-client' => 
    array (
      'pretty_version' => '1.3.11',
      'version' => '1.3.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '2e1a16346f1a9007b389af381135d8fb2e44eddf',
    ),
    'easyswoole/log' => 
    array (
      'pretty_version' => '1.0.5',
      'version' => '1.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '56134136afbccee1ca72a51e24dc454e39a079f2',
    ),
    'easyswoole/mysqli' => 
    array (
      'pretty_version' => '2.2.6',
      'version' => '2.2.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '177579daeaa3824cdf043035121224870fae57f3',
    ),
    'easyswoole/redis' => 
    array (
      'pretty_version' => '1.1.14',
      'version' => '1.1.14.0',
      'aliases' => 
      array (
      ),
      'reference' => '2c037b46c65576f86755827852a1f04f06d9a658',
    ),
    'easyswoole/session' => 
    array (
      'pretty_version' => '2.0.7',
      'version' => '2.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b3b5732e9f1d769c515905cab0d78f0a2148ba5b',
    ),
    'easyswoole/spl' => 
    array (
      'pretty_version' => '1.3.7',
      'version' => '1.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b9d74dfbeb59086c51616ba09d7728cea7351d05',
    ),
    'easyswoole/task' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3694bab2ba614c8d78c4930c721c934635b1c9e3',
    ),
    'easyswoole/trigger' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f449f974d6645342134f455ce4a6885c8ad3a92e',
    ),
    'easyswoole/utility' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cf5d157143c7cbdfdebaa6106cf19663b05e91b8',
    ),
    'easyswoole/validate' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f92fc14a81fbd8512713e8da9e9a509e982048ed',
    ),
    'easyswoole/wechat' => 
    array (
      'pretty_version' => 'v1.0.11',
      'version' => '1.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '370c49af7487bdb34933a84a68741b746901d018',
    ),
    'guanxi/wechat-mp-message-handler' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'jeremeamia/superclosure' => 
    array (
      'pretty_version' => '2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5707d5821b30b9a07acfb4d76949784aaa0e9ce9',
    ),
    'm1/env' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5c296e3e13450a207e12b343f3af1d7ab569f6f3',
    ),
    'nikic/fast-route' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '181d480e08d9476e61381e04a71b34dc0432e812',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.3.0',
      'version' => '4.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9a9981c347c5c49d6dfe5cf826bb882b824080dc',
    ),
    'opis/closure' => 
    array (
      'pretty_version' => '3.5.1',
      'version' => '3.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '93ebc5712cdad8d5f489b500c59d122df2e53969',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'symfony/polyfill-php56' => 
    array (
      'pretty_version' => 'v1.15.0',
      'version' => '1.15.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd51ec491c8ddceae7dca8dd6c7e30428f543f37d',
    ),
    'symfony/polyfill-util' => 
    array (
      'pretty_version' => 'v1.15.0',
      'version' => '1.15.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd8e76c104127675d0ea3df3be0f2ae24a8619027',
    ),
  ),
);
