<?php
/**
 * Created by PhpStorm.
 * User: yf
 * Date: 2018-12-29
 * Time: 21:11
 */

namespace EasySwoole\WeChat\OfficialAccount;


use EasySwoole\Component\Event;
use EasySwoole\WeChat\Bean\OfficialAccount\RequestConst;

class EventContainer extends Event
{
    const EVENT_ENCRYPTOR_DECRYPT = 'event_encryptor_decrypt';
    const EVENT_ENCRYPTOR_ENCRYPT = 'event_encryptor_encrypt';

    // 添加中文注释后
function onSubscribe(callable $call): EventContainer
    {
        // 设置订阅事件
        $this->set(RequestConst::EVENT_SUBSCRIBE, $call);
        return $this;
    }

    function onUnSubscribe(callable $call): EventContainer
    {
        // 设置取消订阅事件
        $this->set(RequestConst::EVENT_UNSUBSCRIBE, $call);
        return $this;
    }

    function onScan(callable $call): EventContainer
    {
        // 设置扫描事件
        $this->set(RequestConst::EVENT_SCAN, $call);
        return $this;
    }

    function onLocation(callable $call): EventContainer
    {
        // 设置地理位置事件
        $this->set(RequestConst::EVENT_LOCATION, $call);
        return $this;
    }

    function onClick(callable $call): EventContainer
    {
        // 设置点击事件
        $this->set(RequestConst::EVENT_CLICK, $call);
        return $this;
    }

    function onView(callable $call): EventContainer
    {
        // 设置查看事件
        $this->set(RequestConst::EVENT_VIEW, $call);
        return $this;
    }

    function onEncryptorDecrypt(callable $call): EventContainer
    {
        // 设置加密解密事件
        $this->set(self::EVENT_ENCRYPTOR_DECRYPT, $call);
        return $this;
    }

    function onEncryptorEncrypt(callable $call): EventContainer
    {
        // 设置加密加密事件
        $this->set(self::EVENT_ENCRYPTOR_ENCRYPT, $call);
        return $this;
    }

    function onSent(callable $call): EventContainer
    {
        $this->set(RequestConst::EVENT_SUBSCRIBE_MSG_POPUP_EVENT, $call);
        return $this;
    }
    function onTwoSent(callable $call): EventContainer
    {
        $this->set(RequestConst::EVENT_SUBSCRIBE_MSG_SENT_EVENT, $call);
        return $this;
    }
    function onUnSent(callable $call): EventContainer
    {
        $this->set(RequestConst::EVENT_SUBSCRIBE_MSG_CHANGE_EVENT, $call);
        return $this;
    }
}